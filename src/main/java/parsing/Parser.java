package parsing;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Parser {

    public static double getArgument(String string) {
        double dvalue;
        try {
            int ivalue = Integer.parseInt(string);
            dvalue = (double)ivalue;
        } catch (NumberFormatException e) {
            dvalue = Double.parseDouble(string); //if this gets an exception, it is allowed to propagate
        }
        return dvalue;
    }
}
