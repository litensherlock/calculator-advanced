package operators.postfix;

import operators.Operator;

import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Multiply extends Operator {
    @Override
    public void apply(LinkedList<String> stack) {
        String secondArgument = stack.pop();
        double second = getArgument(secondArgument);

        String firstArgument = stack.pop();
        double first = getArgument(firstArgument);

        stack.push(String.valueOf(first*second));
    }
}
