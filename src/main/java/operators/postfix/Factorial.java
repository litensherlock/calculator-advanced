package operators.postfix;

import operators.Operator;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Factorial extends Operator {
    @Override
    public void apply(LinkedList<String> stack) {
        String firstArgument = stack.pop();

        double first = getArgument(firstArgument);

        if (isInteger(first)){
            long factorialResult = fact((int) first);

            //adding .0 to let all operations return decimal numbers
            String item = String.valueOf(factorialResult + ".0");
            stack.push(item);
        } else {
            System.err.println("Argument to factorial is not a whole number, ignoring it");
        }
    }

    private boolean isInteger(double first) {
        return Double.isFinite(first) && (first == Math.floor(first));
    }

    private long fact(int n){
        long tot = 1;
        while (n > 1){
            tot *= n;
            --n;
        }
        return tot;
    }
}
