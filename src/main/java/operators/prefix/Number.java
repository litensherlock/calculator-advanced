package operators.prefix;

import operators.Operator;

import java.util.LinkedList;

/**
 * Jakob Lövhall
 * Date: 2017-11-25
 */
public class Number extends Operator {
    private final String value;
    
    public Number(String value) {
        this.value = value;
    }
    
    @Override
    public void apply(LinkedList<String> deque) {
        deque.addFirst(value);
    }
}
