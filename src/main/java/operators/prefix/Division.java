package operators.prefix;

import operators.Operator;
import operators.factories.PrefixOperatorFactory;

import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Division extends Operator {
    private final PrefixOperatorFactory factory;
    
    public Division(PrefixOperatorFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public void apply(LinkedList<String> deque) {
        String firstArgument = deque.removeFirst();
        factory.spawn(firstArgument).apply(deque);
        double first = getArgument(deque.removeFirst());
    
        String secondArgument = deque.removeFirst();
        factory.spawn(secondArgument).apply(deque);
        double second = getArgument(deque.removeFirst());

        deque.addFirst(String.valueOf(first/second));
    }
}
