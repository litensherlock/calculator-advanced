package operators.prefix;

import operators.Operator;
import operators.factories.PrefixOperatorFactory;

import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Factorial extends Operator {
    private final PrefixOperatorFactory factory;
    
    public Factorial(PrefixOperatorFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public void apply(LinkedList<String> deque) {
        String firstArgument = deque.removeFirst();
        factory.spawn(firstArgument).apply(deque);
        double first = getArgument(deque.removeFirst());

        if (isInteger(first)){
            long factorialResult = fact((int) first);

            //adding .0 to let all operations return decimal numbers
            String item = String.valueOf(factorialResult + ".0");
            deque.addFirst(item);
        } else {
            System.err.println("Argument to factorial is not a whole number, ignoring it");
        }
    }

    private boolean isInteger(double first) {
        return Double.isFinite(first) && (first == Math.floor(first));
    }

    private long fact(int n){
        long tot = 1;
        while (n > 1){
            tot *= n;
            --n;
        }
        return tot;
    }
}
