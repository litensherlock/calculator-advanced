package operators.prefix;

import operators.Operator;

import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class Power extends Operator {
    @Override
    public void apply(LinkedList<String> deque) {
        String firstArgument = deque.removeFirst();
        double first = getArgument(firstArgument);
    
        String secondArgument = deque.removeFirst();
        double second = getArgument(secondArgument);

        deque.addFirst(String.valueOf(Math.pow(first,second)));
    }
}
