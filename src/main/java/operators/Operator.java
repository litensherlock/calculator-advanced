package operators;

import parsing.Parser;

import java.util.LinkedList;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public abstract class Operator {
    abstract public void apply(LinkedList<String> strings);

    protected double getArgument(String string) {
        return Parser.getArgument(string);
    }
}
