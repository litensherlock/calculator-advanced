package operators.factories;

import operators.Operator;
import operators.postfix.*;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class PostfixOperatorFactory implements OperatorFactory {
    public Operator spawn(String token){
        if (token.length() > 1){
            System.err.printf("operator string (%s) to long, should be one character. \n", token);
            return null;
        }

        if (token.contains("*")){
            return new Multiply();
        } else if (token.contains("/")){
            return new Division();
        } else if (token.contains("+")){
            return new Addition();
        } else if (token.contains("-")){
             return new Subtraction();
        } else if (token.contains("^")){
            return new Power();
        } else if (token.contains("%")){
            return new Modulo();
        } else if (token.contains("!")){
            return new Factorial();
        }

        return null;
    }
}
