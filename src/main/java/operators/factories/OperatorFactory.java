package operators.factories;

import operators.Operator;

/**
 * Jakob Lövhall
 * Date: 2017-11-25
 */
public interface OperatorFactory {
    Operator spawn(String token);
}
