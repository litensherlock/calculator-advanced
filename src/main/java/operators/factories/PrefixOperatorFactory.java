package operators.factories;

import operators.Operator;
import operators.prefix.*;
import operators.prefix.Number;
import parsing.Parser;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class PrefixOperatorFactory implements OperatorFactory {
    public Operator spawn(String token){
        if (isNumber(token)){
            return new Number(token);
        } else if (token.contains("*")){
            return new Multiply(this);
        } else if (token.contains("/")){
            return new Division(this);
        } else if (token.contains("+")){
            return new Addition(this);
        } else if (token.contains("-")){
             return new Subtraction(this);
        } else if (token.contains("^")){
            return new Power();
        } else if (token.contains("%")){
            return new Modulo(this);
        } else if (token.contains("!")){
            return new Factorial(this);
        }

        return null;
    }
    
    private boolean isNumber(String token) {
        try {
            Parser.getArgument(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
