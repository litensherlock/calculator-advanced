package exceptions;

/**
 * Jakob Lövhall
 * Date: 2017-06-11
 */
public class UnknownSymbol extends Exception {
    public UnknownSymbol(String message) {
        super(message);
    }
}
