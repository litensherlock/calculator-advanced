package functions;

import exceptions.UnknownSymbol;
import parsing.Parser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;

/**
 * Created by Jakob Lövhall on 2016-12-31.
 */
public class FunctionHandler {
    public static void handle(LinkedList<String> stack, String token) throws UnknownSymbol {
        nextMethod : for (Method method : Math.class.getDeclaredMethods()) {
            if(method.getName().equals(token) &&
                            method.getReturnType().getName().equals(double.class.getTypeName())){

                Class<?>[] parameterTypes = method.getParameterTypes();

                Double[] params = new Double[parameterTypes.length];

                for (int i = 0; i < parameterTypes.length; i++) {
                    Class<?> parameterType = parameterTypes[i];
                    String simpleName = parameterType.getSimpleName();
                    if (simpleName.equals(double.class.getSimpleName())){
                        String s = stack.get(parameterTypes.length - 1- i);
                        try {
                            double argument = Parser.getArgument(s);
                            params[i] = new Double(argument);
                        } catch (Exception e) {
                            continue nextMethod;
                        }
                    } else {
                        continue nextMethod;
                    }
                }

                try {
                    Object retVal = method.invoke(null, (Object[]) params);
                    Double dVal = (Double)retVal;

                    //success

                    //clean up
                    for (int i = 0; i < params.length; i++) {
                        stack.pop();
                    }

                    stack.push(dVal.toString());
                    return;
                } catch (IllegalAccessException e) {
                    continue nextMethod;
                } catch (InvocationTargetException e) {
                    continue nextMethod;
                }
            }
        }
        throw new UnknownSymbol(token);
    }
}
