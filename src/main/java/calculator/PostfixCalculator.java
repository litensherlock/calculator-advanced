package calculator;

import exceptions.UnknownSymbol;
import functions.FunctionHandler;
import operators.Operator;
import operators.factories.OperatorFactory;
import operators.factories.PostfixOperatorFactory;
import parsing.Parser;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class PostfixCalculator implements Calculator<String> {
    private final String operators = "+-*/^%!";
    private final String functionStuff = "(),";

    @Override
    public String calculate(String math) throws UnknownSymbol {
        String[] tokens = math.split("\\s");
        LinkedList<String> stack = new LinkedList<>();
        OperatorFactory operatorFactory = new PostfixOperatorFactory();

        for (String token : tokens) {
            if (isOperator(token)) {
                Operator op = operatorFactory.spawn(token);
                if (op == null){
                    System.err.printf("could not interpret token, %s \n", token);
                    return null;
                }

                op.apply(stack);
            } else if (isNumber(token)){
                stack.push(token);
            } else { //assume number as default
                FunctionHandler.handle(stack, token);
            }
        }

        return stack.pop();
    }

    private boolean isNumber(String token) {
        try {
            Parser.getArgument(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isOperator(String token){
        return token.length() == 1 && (operators.indexOf(token.charAt(0)) != -1);
    }
}
