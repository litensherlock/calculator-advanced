package calculator;

import AST.Node;
import exceptions.UnknownSymbol;
import parsing.Parser;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import tokens.Number;
import tokens.Operator;
import tokens.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Jakob Lövhall
 * Date: 2017-11-26
 */
public class AstCalculator implements Calculator<Node> {
    /**
     * takes an abstract syntax tree and tries to simplify it by modifying it.
     * @param ast
     * @return a modified ast with the same root node
     * @throws UnknownSymbol
     */
    public Node calculate(Node ast) throws UnknownSymbol {
        final Token token = ast.getToken();
        if (token instanceof Operator) {
            applyOperator(ast);
        }
        return ast;
    }
    
    private void applyOperator(Node node) throws UnknownSymbol {
        final Token operator = node.getToken();
        final LeafVisitor visitor;
        switch (operator.getString()) {
            case "+":
                visitor = new LeafVisitor() {
                    private double value = 0.0;
        
                    @Override
                    public void apply(final Token token) {
                        value += Parser.getArgument(token.getString());
                    }
        
                    @Override
                    public double getValue() {
                        return value;
                    }
                };
                break;
            case "-":
                visitor = new LeafVisitor() {
                    private double value = 0.0;
                    private boolean lhs = true;
                    private int counter = 0;
        
                    @Override
                    public void apply(final Token token) {
                        if (counter > 2) {
                            throw new RuntimeException("subtraction is a binary operation, but was called with more than two arguments");
                        }
            
                        if (lhs) {
                            lhs = false;
                            value = Parser.getArgument(token.getString());
                        } else {
                            value -= Parser.getArgument(token.getString());
                        }
                        counter++;
                    }
        
                    @Override
                    public double getValue() {
                        return value;
                    }
                };
                break;
            case "*":
                visitor = new LeafVisitor() {
                    private double value = 1.0;
        
                    @Override
                    public void apply(final Token token) {
                        value *= Parser.getArgument(token.getString());
                    }
        
                    @Override
                    public double getValue() {
                        return value;
                    }
                };
                break;
            case "/":
                visitor = new LeafVisitor() {
                    private double value = 0.0;
                    private boolean lhs = true;
                    private int counter = 0;
                    @Override
                    public void apply(final Token token) {
                        if (counter > 2) {
                            throw new RuntimeException("division is a binary operation, but was called with more than two arguments");
                        }
        
                        if (lhs) {
                            lhs = false;
                            value = Parser.getArgument(token.getString());
                        } else {
                            value /= Parser.getArgument(token.getString());
                        }
                        counter++;
                    }
    
                    @Override
                    public double getValue() {
                        return value;
                    }
                };
                break;
            case "^":
                visitor = new LeafVisitor() {
                    private double value = 0.0;
                    private boolean lhs = true;
                    private int counter = 0;
                    @Override
                    public void apply(final Token token) {
                        if (counter > 2) {
                            throw new RuntimeException("power is a binary operation, but was called with more than two arguments");
                        }
            
                        if (lhs) {
                            lhs = false;
                            value = Parser.getArgument(token.getString());
                        } else {
                            value = Math.pow(value, Parser.getArgument(token.getString()));
                        }
                        counter++;
                    }
        
                    @Override
                    public double getValue() {
                        return value;
                    }
                };
                break;
            case "%":
                throw new NotImplementedException();
            default:
                throw new UnknownSymbol(operator.getString());
        }
        performOperation(node, visitor);
    }
    
    interface LeafVisitor {
        void apply(Token token);
        double getValue();
    }
    
    private void performOperation(Node node, LeafVisitor visitor) throws UnknownSymbol {
        final List<Node> children = node.getChildren();
        final List<Node> uncalculatable = new ArrayList<>(children.size());
        for (Node child : children) {
            if (!child.getChildren().isEmpty()) { //not a leaf node
                calculate(child);
            }
            
            //is it a leaf node now?
            if (child.getChildren().isEmpty()) {
                try {
                    visitor.apply(child.getToken());
                } catch (Exception e) {
                    uncalculatable.add(child);
                }
            } else {
                uncalculatable.add(child);
            }
        }
        if (!uncalculatable.isEmpty()) {
            return;
        }
        
        children.clear();
        Number number = new Number(Double.toString(visitor.getValue()));
    
        //is this a leaf node now?
        if (children.isEmpty()) { //yes, transform into a number instead of a operator
            node.setToken(number);
        } else {
            Node e = new Node(number);
            children.add(e);
        }
    }
}
