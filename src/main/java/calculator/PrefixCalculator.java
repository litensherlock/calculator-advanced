package calculator;

import exceptions.UnknownSymbol;
import functions.FunctionHandler;
import operators.Operator;
import operators.factories.OperatorFactory;
import operators.factories.PrefixOperatorFactory;
import parsing.Parser;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class PrefixCalculator implements Calculator<String>{
    private final String operators = "+-*/^%!";
    
    @Override
    public String calculate(final String math) throws UnknownSymbol {
        final String[] tokens = math.split("\\s");
        final LinkedList<String> queue = new LinkedList<>();
        queue.addAll(Arrays.asList(tokens));
        OperatorFactory operatorFactory = new PrefixOperatorFactory();
        
        while (!queue.isEmpty() && queue.size() > 1) {
            String token = queue.remove();
            Operator op;
            if ((op = operatorFactory.spawn(token)) != null) {
                op.apply(queue);
            }
        }
        
        return queue.pop();
    }
    
    private boolean isNumber(String token) {
        try {
            Parser.getArgument(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    private boolean isOperator(String token){
        return token.length() == 1 && (operators.indexOf(token.charAt(0)) != -1);
    }
}
