package calculator;

import exceptions.UnknownSymbol;
import shunting_yard.ShuntingYard;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class InfixCalculator extends PostfixCalculator{
    @Override
    public String calculate(String math) throws UnknownSymbol {
        String postfix = ShuntingYard.infixToPostfix(math);
        return super.calculate(postfix);
    }
}
