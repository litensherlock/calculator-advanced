package calculator;

import exceptions.UnknownSymbol;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public interface Calculator<T> {
    T calculate(T math) throws UnknownSymbol;
}
