import calculator.InfixCalculator;
import exceptions.UnknownSymbol;

import static org.junit.Assert.*;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class InfixCalculatorTest {
    @org.junit.Test
    public void calculate1() throws Exception {
        String input = "3+4-1+2*2";
        String expected = "10.0";

        assertEquals(expected, new InfixCalculator().calculate(input));
    }

    @org.junit.Test
    public void calculate2() throws Exception {
        String input = "( 3 + 4 /2 - 1 + 2 * 2)^ 2";
        String expected = "64.0";

        assertEquals(expected, new InfixCalculator().calculate(input));
    }

    @org.junit.Test
    public void calculateFunction() throws Exception {
        String input = "pow( max( 2 , 3) ,min( 7 ,2 ))";
        String expected = "9.0";

        assertEquals(expected, new InfixCalculator().calculate(input));
    }

    @org.junit.Test
    public void modulo() throws Exception {
        String input = "3 % 2";
        String expected = "1.0";

        assertEquals(expected, new InfixCalculator().calculate(input));
    }

    @org.junit.Test
    public void factorial() throws Exception {
        String input = "1 + 3 ! + 1";
        String expected = "8.0";

        assertEquals(expected, new InfixCalculator().calculate(input));
    }
    
    @org.junit.Test
    public void unknownSymbol() throws Exception {
        String input = "1 + 3 ! + arcsin(1)";
        
        try{
            new InfixCalculator().calculate(input);
            assertTrue(false);
        } catch (UnknownSymbol e){
            assertTrue(true);
        }
    }
}