import calculator.PrefixCalculator;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jakob L�vhall on 2016-12-31.
 */
public class PrefixCalculatorTest {
    @org.junit.Test
    public void calculate1() throws Exception {
        String input = "+ 3 - 4 + 1 * 2 2";
        String expected = "2.0";

        assertEquals(expected, new PrefixCalculator().calculate(input));
    }
    
    @org.junit.Test
    public void calculate2() throws Exception {
        String input = "+ + 3 - 4 1 * 2 2";
        String expected = "10.0";

        assertEquals(expected, new PrefixCalculator().calculate(input));
    }
    
    @org.junit.Test
    public void calculate3() throws Exception {
        String input = "- * / 15 - 7 + 1 1 3 + 2 + 1 1";
        String expected = "5.0";
        
        assertEquals(expected, new PrefixCalculator().calculate(input));
    }
    
    @org.junit.Test
    @Ignore
    public void variables() throws Exception {
        String input = "* - + * x 5 + 3 4 - 5 5 * c x";
        String expected = "* - + * x 5 7 0 * c x";
        
        assertEquals(expected, new PrefixCalculator().calculate(input));
    }
}